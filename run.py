import multiprocessing

from fractal_parameters.GUI import App

# Command that need to be invoked in run.py directory in order to convert program to one-folder bundle containing an
# executable. It can be run without installed python but requires installation of pyinstaller
# https://pyinstaller.readthedocs.io/en/stable/installation.html

"""pyinstaller -w --onedir --icon=logo.ico --additional-hooks-dir=. --collect-all ttkbootstrap --add-data 
"logo.ico;." --add-data "fractal_parameters/GUI/README.md;fractal_parameters/GUI" run.py"""

if __name__ == "__main__":
    multiprocessing.freeze_support()
    app = App()
    app.mainloop()
