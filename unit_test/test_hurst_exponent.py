from math import isclose, sqrt

from PIL import Image

from fractal_parameters.parameters import hurst_exponent


def test_hurst_exponent(black_image: Image):
    expected, _ = hurst_exponent(None, black_image)
    actual = 0.5
    uncertainty = 0.1 / sqrt(3)

    assert isclose(actual, expected, rel_tol=2 * uncertainty)
