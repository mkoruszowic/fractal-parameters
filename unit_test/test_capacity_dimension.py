from math import isclose

from PIL import Image

from fractal_parameters.parameters import capacity_fractal_dimension
from fractal_parameters.tools import rnd, find_linear_region


def test_capacity_fractal_dimension(black_image: Image) -> None:
    """
    Expected values based on Expected values based on http://paulbourke.net/fractals/fracdim/
    """
    _, row = black_image.size

    start_step = rnd(row / 2)
    rng = range(start_step, 0, -1)
    x_list = []
    y_list = []

    for box_size in rng:
        _, x, y = capacity_fractal_dimension(box_size, black_image)
        x_list.append(x)
        y_list.append(y)

    linear_region = find_linear_region(x_list, y_list)

    actual = linear_region[0]
    expected = 2
    uncertainty = 2 * linear_region[2]

    assert isclose(actual, expected, rel_tol=uncertainty)
