import os

import pytest
from PIL import Image

from fractal_parameters import ImageEditor


def resolve_path(base_path: str, path: str) -> str:
    return os.path.join(os.path.dirname(base_path), path)


@pytest.fixture
def black_image() -> Image:
    fractal = ImageEditor.from_binary(resolve_path(__file__, os.path.join('testing_samples', 'black.png')))
    fractal.invert_image_color()
    return fractal.image


@pytest.fixture
def carpet_image() -> Image:
    fractal = ImageEditor.from_binary(resolve_path(__file__, os.path.join('testing_samples', 'carpet_216.png')))
    fractal.invert_image_color()
    return fractal.image


@pytest.fixture
def lacunarity_image() -> Image:
    fractal = ImageEditor.from_binary(resolve_path(__file__, os.path.join('testing_samples', '1a_5x5.png')))
    fractal.invert_image_color()
    return fractal.image


@pytest.fixture
def succolarity_fractal_object() -> ImageEditor:
    fractal = ImageEditor.from_binary(resolve_path(__file__, os.path.join('testing_samples', 'succolarity_9x9.png')))
    return fractal


@pytest.fixture
def topothesy_image() -> Image:
    fractal = ImageEditor(resolve_path(__file__, os.path.join('testing_samples', 'topothesy_test.png')))
    return fractal.image
