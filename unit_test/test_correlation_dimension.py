from math import isclose, sqrt

from PIL import Image

from fractal_parameters.parameters import correlation_fractal_dimension
from fractal_parameters.tools import rnd, find_linear_region


def test_correlation_fractal_dimension(black_image: Image) -> None:
    """
    Expected values based on Expected values based on http://paulbourke.net/fractals/fracdim/
    """
    col, row = black_image.size

    r_max = rnd(sqrt(row * row + col * col))
    rng = range(3, r_max + 1)

    x_list = []
    y_list = []

    for box_size in rng:
        x, y = correlation_fractal_dimension(box_size, black_image)
        x_list.append(x)
        y_list.append(y)

    linear_region = find_linear_region(x_list, y_list)

    actual = linear_region[0]
    expected = 2
    uncertainty = 2 * linear_region[2]

    assert isclose(actual, expected, rel_tol=uncertainty)
