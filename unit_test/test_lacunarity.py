from math import isclose

from PIL import Image

from fractal_parameters.parameters.lacunarity import lacunarity, lacunarity_probability_dist


def test_lacunarity(lacunarity_image: Image) -> None:
    """
    Expected values based on DOI:10.1023/A:1008148514268
    """

    actual = lacunarity(2, lacunarity_image)[1]
    expected = 1.95
    uncertainty = 0.01

    assert isclose(actual, expected, rel_tol=uncertainty)


def test_lacunarity_probability_dist(lacunarity_image: Image) -> None:
    """
    Expected values based on DOI:10.1023/A:1008148514268
    """

    actual = lacunarity_probability_dist(2, lacunarity_image)[1]
    expected = 1.95
    uncertainty = 0.01

    assert isclose(actual, expected, rel_tol=uncertainty)
