from math import isclose
from typing import Tuple

import pytest
from PIL import Image

from fractal_parameters import tools
from fractal_parameters.calculator import FractalParamCalculator
from fractal_parameters.parameters import topothesy


# No longer used
@pytest.mark.parametrize('sub_image, fractal_dimension, expected_topothesy', [
    ((0, 0), 1.1813, 193.770),
    ((3, 0), 2.6008, 2.1444),
    ((0, 3), 2.161, 2.9677),
    ((3, 3), 2.3847, 2.8818)
])
def test_topothesy_OLD(sub_image: Tuple[int, int], expected_topothesy: int, fractal_dimension: int,
                       topothesy_image: FractalParamCalculator) -> None:
    """
    Expected values based on DOI 10.1007/s11760-013-0604-5
    """

    ln_tau, ln_values = topothesy.structure_function_OLD(3, topothesy_image, *sub_image)
    coefficients, _ = tools.linear_regression(ln_tau, ln_values)
    actual = topothesy.determine_topothesy(*coefficients)
    uncertainty = 0.001

    assert isclose(actual[0], expected_topothesy, rel_tol=uncertainty)
    assert isclose(actual[1], fractal_dimension, rel_tol=uncertainty)


@pytest.mark.parametrize('coordinates, fractal_dimension, expected_topothesy', [
    ((0, 0), 1.1813, 193.770),
    ((3, 0), 2.6008, 2.1444),
    ((0, 3), 2.161, 2.9677),
    ((3, 3), 2.3847, 2.8818)
])
def test_topothesy(coordinates: Tuple[int, int], expected_topothesy: int, fractal_dimension: int,
                   topothesy_image: Image) -> None:
    """
    Expected values based on DOI 10.1007/s11760-013-0604-5
    """
    m = 3
    x, y = coordinates
    area = (x, y, x + m, y + m)
    sub_image = topothesy_image.crop(area)
    tau_data = []
    struct_fun_data = []

    for tau in range(1, m):
        _, ln_tau, ln_struct_fun = topothesy.structure_function(tau, sub_image)
        tau_data.append(ln_tau)
        struct_fun_data.append(ln_struct_fun)

    coefficients, _ = tools.linear_regression(tau_data, struct_fun_data)
    actual = topothesy.determine_topothesy(*coefficients)
    uncertainty = 0.001

    assert isclose(actual[0], expected_topothesy, rel_tol=uncertainty)
    assert isclose(actual[1], fractal_dimension, rel_tol=uncertainty)
