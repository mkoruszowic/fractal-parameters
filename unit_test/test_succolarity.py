from math import isclose
from typing import Tuple

import pytest

from fractal_parameters import ImageEditor
from fractal_parameters.parameters import succolarity


@pytest.mark.parametrize('angle, expected', [
    ('l2r', (0.0384, 0.0576)),  # l2r
    ('r2l', (0.4829, 0.4691)),  # ! r2l
    ('t2b', (0.2387, 0.2634)),  # t2b
    ('b2t', (0.3429, 0.3292))  # ! b2t
])
def test_succolarity(succolarity_fractal_object: ImageEditor, expected: Tuple[float], angle: int) -> None:
    """
    Expected values based on DOI 10.1007/s11235-011-9657-3
    """

    image = succolarity_fractal_object.flood_image(direction=angle)
    succ_1 = round(succolarity(1, image)[1], 4)
    succ_3 = round(succolarity(3, image)[1], 4)
    uncertainty = 0.0001

    assert isclose(succ_1, expected[0], rel_tol=uncertainty)
    assert isclose(succ_3, expected[1], rel_tol=uncertainty)
