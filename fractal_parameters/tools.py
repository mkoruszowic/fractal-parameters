import math
import os
from typing import Union, Sequence, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.interpolate import UnivariateSpline


def rnd(val: Union[int, float]) -> Union[int, float]:
    """
    Mathematically proper rounding

    :param val: Number to be round
    :return: Rounded number
    """
    if val - math.floor(val) < 0.5:
        return math.floor(val)
    return math.ceil(val)


def display_csv_as_plot(file: str, x_col: int, y_col: int) -> None:
    """
    Plot creation from csv files

    :param file: Filename
    :param x_col: X column number
    :param y_col: Y column number
    """
    x, y, y_label, x_label = read_data(file, x_col, y_col)

    plt.plot(x, y, marker='.', linestyle='-')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    filename = os.path.basename(file).split('.')[0]
    plt.title(filename)
    plt.show()


def linear_regression(x_data: Sequence, y_data: Sequence):
    """
    Linear regression calculation

    :param x_data: X
    :param y_data: Y
    :return: a, b, u(a), u(b)
    """
    if len(x_data) > 2 and len(y_data) > 2:
        result, cov = np.polyfit(x_data, y_data, 1, cov=True)
        uncertainty = np.sqrt(np.diag(cov))
    else:
        result = np.polyfit(x_data, y_data, 1)
        uncertainty = 0, 0

    return result, uncertainty


def read_data(file: str, x_col: int, y_col: int):
    """
    Reading csv files

    :param file: Filename
    :param x_col: X column number
    :param y_col: Y column number
    :return:
    """
    data = pd.read_csv(file)
    headers = data.columns
    x_label = headers[x_col - 1]
    y_label = headers[y_col - 1]
    x = data[x_label]
    y = data[y_label]
    return x, y, y_label, x_label


def derivative(x_data: Sequence, y_data: Sequence) -> Tuple[Sequence, Sequence]:
    y_prime = np.diff(y_data) / np.diff(x_data)
    x_prime = x_data[:len(y_prime)]

    return x_prime, list(y_prime)


def find_linear_region(x_data, y_data):
    threshold = 0.1
    linear_region = []
    temp = []
    spline = UnivariateSpline(x_data, y_data)
    y_spl = spline(x_data)

    x_der, y_der = derivative(x_data, y_spl)
    x_der_2, y_der_2 = derivative(x_der, y_der)

    while len(linear_region) <= 3:
        for i, y in enumerate(y_der_2):
            if abs(y) < threshold:
                temp.append(i)
            else:
                if len(temp) > len(linear_region):
                    linear_region = temp
                else:
                    temp = []

        if len(temp) >= len(linear_region):
            linear_region = temp

        threshold += 0.1  # Increasing threshold in case linear region is empty

    start = linear_region[0]
    end = linear_region[-1] + 1
    res, uncertainty = linear_regression(x_data[start:end], y_spl[start: end])

    return res[0], res[1], uncertainty[0], uncertainty[1]


def find_linear_region_debug(y_data, threshold=0.1):
    linear_region = []
    temp = []
    # max_y = max(y_data)
    # y_data = [y/max_y for y in y_data]

    while len(linear_region) <= 3:
        for i, y in enumerate(y_data):
            if abs(y) < threshold:
                temp.append(i)
            else:
                if len(temp) > len(linear_region):
                    linear_region = temp
                else:
                    temp = []

        if len(temp) >= len(linear_region):
            linear_region = temp

        threshold += 0.1  # Increasing threshold in case linear region is empty

    return linear_region


if __name__ == '__main__':
    # x, y, _, _ = read_data('../Result_A_optymistychna/correlation/A_optymistychna_mono_correlation.csv', 1, 2)
    # x, y, _, _ = read_data('../A_matlab_correlation.csv', 1, 2)
    # x, y, _, _ = read_data('../Result_A_optymistychna/capacity/A_optymistychna_mono_capacity.csv', 2, 3)
    # x, y, _, _ = read_data('../A_matlab_capacity.csv', 2, 3)
    # x, y, _, _ = read_data('../Result_black/correlation/black_mono_correlation.csv', 1, 2)
    # x, y, _, _ = read_data('../Result_black/capacity/black_mono_capacity.csv', 2, 3)
    # x, y, _, _ = read_data('../Result_pan_14_to_gs/topothesy/pan_14_to_gs__topothesy_33_0.csv', 2, 3)
    x, y, _, _ = read_data('../Result_pan_14_to_gs/capacity/pan_14_to_gs_mono_capacity.csv', 2, 3)

    # print(x, y)
    spl = UnivariateSpline(x, y)
    y_sp = spl(x)
    x_p, y_p = derivative(x, y_sp)
    # print(y_p)
    x_prime, y_prime = derivative(x_p, y_p)
    # print(y_prime)
    linear_region_indexes = find_linear_region_debug(y_prime)
    start = linear_region_indexes[0]
    end = linear_region_indexes[-1] + 1
    res, unc = linear_regression(x[start:end], y[start: end])
    res2, unc2 = linear_regression(x[10:1100], y[10:1100])
    print(linear_region_indexes)
    # print(y[start: end])
    # print(y_prime[start: end])
    print(res, unc)
    print(res2, unc2)
    y_lin = x * res[0] + res[1]

    a, b, _, _ = find_linear_region(x, y)
    print(a, b)

    plt.plot(x, y, ".-")
    plt.plot(x, y_lin, "-")
    plt.plot(x, y_sp, '-')
    plt.show()
