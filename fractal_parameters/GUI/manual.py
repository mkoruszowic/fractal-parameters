import os
from tkinter import ttk

from tkhtmlview import HTMLScrolledText

from fractal_parameters.GUI.tools import convert_markdown_to_html


class ManualFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.grid_propagate(0)
        html = convert_markdown_to_html(os.path.join('fractal_parameters', 'GUI', 'README.md'))
        text = HTMLScrolledText(self, html=html, state='disabled', background='#ADB5BD')
        text.grid(column=0, row=0, sticky='nsew', padx=5, pady=5)
