import tkinter.font as tk_font

import ttkbootstrap


class AppStyle(ttkbootstrap.Style):
    def __init__(self, parent):
        super().__init__(parent)
        self.theme_use('superhero')

        self.default_font = tk_font.Font(family="Helvetica", size=12)
        self.configure('TRadiobutton', font=self.default_font)
        self.configure('TButton', font=self.default_font)
        self.configure('.', font=self.default_font)
