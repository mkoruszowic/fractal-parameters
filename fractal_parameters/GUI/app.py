import os
import sys
import tkinter as tk
from tkinter import ttk

from fractal_parameters.GUI.calculator import Calculator
from fractal_parameters.GUI.manual import ManualFrame
from fractal_parameters.GUI.plot_creator import PlotCreator
from fractal_parameters.GUI.styles import AppStyle


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS

    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title('Fractal parameter calculator')
        self.style = AppStyle(self)
        self.option_add("*font", self.style.default_font)

        window_width = 1000
        window_height = 500

        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)

        self.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')
        self.resizable(False, False)

        self.iconbitmap('logo.ico')

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        notebook = ttk.Notebook(self)
        notebook.grid(row=0, column=0, sticky="nsew")

        self.calculator_frame = Calculator(notebook)
        self.calculator_frame.grid(row=0, column=0, sticky="nsew")

        self.plot_creator = PlotCreator(notebook)
        self.plot_creator.grid(row=0, column=0, sticky="nsew")

        self.help_frame = ManualFrame(self)
        self.help_frame.grid(row=0, column=0, sticky="nsew")

        notebook.add(self.calculator_frame, text='Calculator')
        notebook.add(self.plot_creator, text='Plot creator')
        notebook.add(self.help_frame, text='Manual')
