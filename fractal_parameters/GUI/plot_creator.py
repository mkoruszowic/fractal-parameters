import os
import tkinter as tk
from tkinter import filedialog as fd
from tkinter import ttk as ttk
from tkinter.messagebox import showwarning
from tkinter.scrolledtext import ScrolledText
from typing import Sequence, Optional, Tuple

import matplotlib
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg,
    NavigationToolbar2Tk
)
from matplotlib.figure import Figure
from pandas.errors import EmptyDataError

import fractal_parameters.tools as tool
from fractal_parameters.parameters import determine_topothesy

matplotlib.use('TkAgg')

PAD_X = 5
PAD_Y = 5


class PlotCreator(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)

        self.columnconfigure(0, weight=2)
        self.columnconfigure(1, weight=3)
        self.rowconfigure(0, weight=1)
        self.grid_propagate(0)

        self.fitting_frame = FittingFrame(self)
        self.fitting_frame.grid(row=0, column=0, sticky='nsew', padx=PAD_X, pady=PAD_Y)

        self.plot_frame = PlotFrame(self)
        self.plot_frame.grid(row=0, column=1, sticky='nsew', padx=PAD_X, pady=PAD_Y)

    def browse_files(self):
        file = fd.askopenfilename(title="Select a File", filetypes=(('Comma-separated values', '*.csv'),
                                                                    ('All files', '*.*')))
        if file:
            filename = os.path.basename(file).split('.')[0]
            if 'hurst' not in filename:
                if 'correlation' in filename:
                    data_indicator = 1, 2
                elif 'capacity' in filename:
                    data_indicator = 2, 3
                elif 'lacunarity' in filename:
                    data_indicator = 3, 4
                elif 'succolarity' in filename:
                    data_indicator = 1, 2
                elif 'topothesy' in filename:
                    data_indicator = 2, 3
                else:
                    showwarning(title='Warning', message='Not supported file!\nYou can only open files generated from '
                                                         'this program')
                    return

                x_col, y_col = data_indicator

                try:
                    x, y, y_label, x_label = tool.read_data(file, x_col, y_col)

                except EmptyDataError:
                    showwarning(title='Warning', message='Empty file')
                    return

                self.fitting_frame.filename = filename
                self.fitting_frame.x_data = x
                self.fitting_frame.y_data = y

                try:
                    self.fitting_frame.lower_limit_value['from_'] = min(x)
                    self.fitting_frame.lower_limit_value['to'] = max(x)

                    self.fitting_frame.upper_limit_value['from_'] = min(x)
                    self.fitting_frame.upper_limit_value['to'] = max(x)

                except tk.TclError:
                    showwarning(title='Warning', message='Not supported format')
                    return

                except ValueError:
                    showwarning(title='Warning', message='Wrong value')
                    return

                if (self.fitting_frame.lower_limit_value['value'] < min(x) or
                        self.fitting_frame.lower_limit_value['value'] > max(x)):
                    self.fitting_frame.lower_limit_value['value'] = min(x)
                    self.fitting_frame.update_lower_limit(None)

                if (self.fitting_frame.upper_limit_value['value'] > max(x) or
                        self.fitting_frame.upper_limit_value['value'] < min(x)):
                    self.fitting_frame.upper_limit_value['value'] = max(x)
                    self.fitting_frame.update_upper_limit(None)

                self.fitting_frame.lower_limit_value.state(["!disabled"])
                self.fitting_frame.upper_limit_value.state(["!disabled"])
                self.fitting_frame.clear_text['state'] = 'normal'
                self.fitting_frame.save_as_results['state'] = 'normal'
                self.plot_frame.create_plot(filename, x, y, y_label, x_label)

                if 'topothesy' in filename:
                    self.fitting_frame.calculate_regression_button['state'] = 'disabled'
                    self.fitting_frame.calculate_topothesy_button['state'] = 'normal'
                else:
                    self.fitting_frame.calculate_regression_button['state'] = 'normal'
                    self.fitting_frame.calculate_topothesy_button['state'] = 'disabled'

            else:
                try:
                    x, _, _, _ = tool.read_data(file, 1, 2)
                except EmptyDataError:
                    showwarning(title='Warning', message='Empty file')
                    return
                self.plot_frame.create_hist(filename, x)
                self.fitting_frame.disable()

            self.plot_frame.label_file_explorer['text'] = "Opened file: " + filename


class PlotFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.widget = None

        self.columnconfigure(0, weight=4)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)
        self.grid_propagate(0)

        button_file_explorer = ttk.Button(self, text='Browse files', command=master.browse_files)
        button_file_explorer.grid(row=0, column=1, sticky='nsew')

        self.label_file_explorer = ttk.Label(self, text='Select file', width=1)
        self.label_file_explorer.grid(row=0, column=0, sticky='nsew')

    def _plot(self) -> Figure:
        figure = Figure(figsize=(1, 1), dpi=100)
        figure.subplots_adjust(bottom=0.148, top=0.898)

        figure_canvas = FigureCanvasTkAgg(figure, self)

        toolbar = NavigationToolbar2Tk(figure_canvas, self)
        toolbar.grid(row=2, column=0, columnspan=2, sticky='nsew')
        toolbar.update()

        self.widget = figure_canvas.get_tk_widget()
        self.widget['height'] = 300
        self.widget.grid(row=1, column=0, columnspan=2, sticky='nsew', pady=PAD_Y)
        self.widget.propagate(0)
        return figure

    def create_plot(self, filename: str, x: Sequence, y: Sequence, y_label: str, x_label: str) -> None:
        figure = self._plot()

        axes = figure.add_subplot()
        axes.plot(x, y, '.-')
        axes.set_title(filename)
        axes.set_xlabel(x_label)
        axes.set_ylabel(y_label)

    def create_hist(self, filename: str, x: Sequence) -> None:
        figure = self._plot()

        axes = figure.add_subplot()
        axes.hist(x, bins=50, edgecolor='black')
        axes.set_title(filename)
        axes.set_xlabel('Hurst exponent')
        axes.set_ylabel('Counts')


class FittingFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)

        self.filename = None
        self.x_data = None
        self.y_data = None
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(7, weight=1)
        self.grid_propagate(0)

        linear_range = ttk.Label(self, text='Select linear region based on x value')
        linear_range.grid(row=0, column=0, columnspan=2, sticky='w', pady=(PAD_Y, PAD_Y + 10))

        self.lower_limit_value = ttk.Scale(self, orient='horizontal', command=self.update_lower_limit)
        self.lower_limit_value.state(["disabled"])
        self.lower_limit_value.grid(row=2, column=0, columnspan=2, sticky='nsew', pady=(0, PAD_Y))

        self.lower_limit_label = ttk.Label(self, text=f'From {self.lower_limit_value.get()}')
        self.lower_limit_label.grid(row=1, column=0, columnspan=2, sticky='nsew')

        self.upper_limit_value = ttk.Scale(self, orient='horizontal', command=self.update_upper_limit)
        self.upper_limit_value.state(["disabled"])
        self.upper_limit_value.grid(row=4, column=0, columnspan=2, sticky='nsew', pady=(0, PAD_Y))

        self.upper_limit_label = ttk.Label(self, text=f'To {self.upper_limit_value.get()}')
        self.upper_limit_label.grid(row=3, column=0, columnspan=2, sticky='nsew')

        self.calculate_regression_button = ttk.Button(self, text='Calculate regression',
                                                      command=self.calculate_regression, state='disabled')
        self.calculate_regression_button.grid(row=5, column=0, sticky='new', padx=(0, PAD_X), pady=(PAD_Y + 10, 0))

        self.calculate_topothesy_button = ttk.Button(self, text='Calculate topothesy',
                                                     command=self.calculate_topothesy, state='disabled')
        self.calculate_topothesy_button.grid(row=5, column=1, sticky='new', pady=(PAD_Y + 10, 0))

        self.results_label = ttk.Label(self, text='Results', state='disabled')
        self.results_label.grid(row=6, column=0, sticky='w', columnspan=2, pady=PAD_Y)

        self.results = ScrolledText(self)
        self.results.insert(1.0, '...')
        self.results.grid(row=7, column=0, sticky='nsew', columnspan=2, pady=(0, PAD_Y + 10))

        self.clear_text = ttk.Button(self, text='Clear text', command=self.clear, state='disabled')
        self.clear_text.grid(row=8, column=0, sticky='nsew', padx=(0, PAD_X))

        self.save_as_results = ttk.Button(self, text='Save as', command=self.save_results_as, state='disabled')
        self.save_as_results.grid(row=8, column=1, sticky='nsew')

    def disable(self):
        self.lower_limit_value['state'] = 'disabled'
        self.lower_limit_value['value'] = 0
        self.update_lower_limit(None)

        self.upper_limit_value['state'] = 'disabled'
        self.upper_limit_value['value'] = 0
        self.update_upper_limit(None)

        self.clear_text['state'] = 'disabled'
        self.save_as_results['state'] = 'disabled'

        self.calculate_regression_button['state'] = 'disabled'
        self.calculate_topothesy_button['state'] = 'disabled'

    def update_lower_limit(self, event):
        self.lower_limit_label['text'] = f'From {self.lower_limit_value.get():.2f}'

    def update_upper_limit(self, event):
        self.upper_limit_label['text'] = f'To {self.upper_limit_value.get():.2f}'

    def clear(self):
        self.results.delete(1.0, 'end')

    def save_results_as(self):
        file = fd.asksaveasfile(initialfile='linear_regression.csv', defaultextension=".txt",
                                filetypes=[("Comma separated values", "*.csv"), ("Text Documents", "*.txt")])
        if file:
            file.write(self.results.get('1.0', tk.END))

    def calculate_regression(self):
        results = self._linear_regression()
        if results:
            a, b, u_a, u_b = results
        else:
            return

        self._check_first_line()

        self.results.insert(tk.END, f'\n{a:.4f},{b:.4f},{u_a:.4f},{u_b:.4f},{self.filename}')

    def calculate_topothesy(self):
        results = self._linear_regression()
        if results:
            a, b, u_a, u_b = results
        else:
            return

        topothesy, fractal_dimension = determine_topothesy(a, b)

        self._check_first_line()
        self.results.insert(tk.END, f'\n{topothesy:.4f},{fractal_dimension:.4f},topothesy,fractal dimension,'
                                    f'{self.filename}')

    def _check_first_line(self):
        if self.results.get('1.0', tk.END).split('\n')[0] != f'a,b,u(a),u(b),filename':
            self.results.delete(1.0, 'end')
            self.results.insert(1.0, f'a,b,u(a),u(b),filename')

    def _linear_regression(self) -> Optional[Tuple]:
        lower_limit = self.lower_limit_value.get()
        upper_limit = self.upper_limit_value.get()

        if lower_limit > upper_limit:
            showwarning(title='Warning', message="Lower limit must be smaller than upper!")
            return None

        x = self.x_data[(lower_limit <= self.x_data) & (self.x_data <= upper_limit)]
        y = self.y_data[x.index]

        res, uncertainty = tool.linear_regression(x, y)
        a, b = res
        u_a, u_b = uncertainty
        return a, b, u_a, u_b
