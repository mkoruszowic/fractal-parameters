from tkinter.messagebox import showwarning
from typing import Optional

import markdown as md


def validate_range(value: int, lower: int, upper: int, warning: str) -> Optional[bool]:
    if lower <= value <= upper:
        return True
    else:
        showwarning(title='Warning', message=f'{warning} value out of range')


def validate_number(num):
    try:
        int(num)
        return True
    except ValueError:
        return False


def convert_markdown_to_html(filename):
    with open(filename, 'r') as file:
        text = file.read()
        html = md.markdown(text)

    return html
