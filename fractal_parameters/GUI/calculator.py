import sys
import tkinter as tk
from threading import Thread
from tkinter import filedialog as fd
from tkinter import ttk as ttk
from tkinter.messagebox import askokcancel, showwarning
from typing import Optional, Callable

from PIL import ImageTk, Image

from fractal_parameters.GUI.tools import validate_number, validate_range
from fractal_parameters.calculator import FractalParamCalculator, channel_divisor

PAD_X = 5
PAD_Y = 5


class Calculator(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)

        self.columnconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.option_frame = OptionFrame(self)
        self.option_frame.grid(row=0, column=0, sticky='nsew', padx=PAD_X, pady=PAD_Y)

        self.image_frame = ImageFrame(self)
        self.image_frame.grid(row=0, column=1, sticky='nsew', padx=PAD_X, pady=PAD_Y)

    def browse_files(self):
        filename = fd.askopenfilename(title="Select a File", filetypes=(('Images', '*.png *.jpg *jpeg *bmp'),
                                                                        ('All files', '*.*')))
        max_size = 350
        min_size = 300

        if filename:
            self.option_frame.filename = filename
            image = Image.open(filename, 'r')

            # todo: check it only for parameters for which it's necessary
            if image.width != image.height:
                showwarning(title='Warning', message='Image needs to be square!')
                return

            if not min_size < max(image.size) < max_size:
                multiplier = max_size / max(image.size)
                width = int(image.width * multiplier)
                height = int(image.height * multiplier)
                self.image_frame.fractal = image.resize((width, height), resample=Image.BOX)
            else:
                self.image_frame.fractal = image

            self.image_frame.normal_button['state'] = 'normal'
            self.image_frame.greyscale_button['state'] = 'normal'
            self.image_frame.normal_button.invoke()

            self.image_frame.label_file_explorer.configure(text="Opened file: " + filename.split('/')[-1])

            self.option_frame.calculate_button['state'] = 'normal'
            self.option_frame.parameters_frame.update_spinbox(image)


class OptionFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.image: Optional[FractalParamCalculator] = None
        self.columnconfigure(0, weight=3)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)

        self.options = {}
        self.filename: Optional[str] = None

        self.grid_propagate(0)

        self.material_frame = MaterialFrame(self)
        self.material_frame.grid(row=0, column=1, sticky='nsew', padx=(PAD_X, 0), pady=PAD_Y)

        self.channels_frame = ChannelsFrame(self)
        self.channels_frame.grid(row=0, column=0, sticky='nsew', pady=PAD_Y)

        self.parameters_frame = ParametersFrame(self)
        self.parameters_frame.grid(row=2, column=0, columnspan=2, sticky='nsew', pady=PAD_Y)

        self.calculate_button = ttk.Button(self, text='Calculate', command=self.calculate_parameters, state='disabled')
        self.calculate_button.grid(row=3, column=0, columnspan=2, sticky='sew')

        self.progress_window: Optional[ProgressWindow] = None
        self.progress_step = 0

    def calculate_parameters(self) -> None:
        number_of_parameters = len([value for value in self.parameters_frame.states.values() if value.get()])

        sub_images_num = 0

        if number_of_parameters and self._validate_spinbox():

            if self.parameters_frame.states['topothesy'].get():
                sub_images_num = self._validate_topothesy()
                if not sub_images_num:
                    return

            number_of_channels = self.channels_frame.number_of_channels_value.get()
            material = self.material_frame.material_indicator.get()
            self.progress_window = ProgressWindow(self)
            self.progress_window.lift()
            self.update()

            if number_of_channels != 1:
                for num, threshold in enumerate(channel_divisor(number_of_channels)):
                    lower, upper = threshold
                    image = FractalParamCalculator.to_binary_channel(self.filename, lower, upper)
                    self.progress_window.update_channel_label(f'Channel {num + 1}')
                    self._calculate_with_progressbar(image, self._calculate, image, material)
                    if self.progress_window.progress_worker.killed:
                        break
            else:
                image = FractalParamCalculator.from_binary(self.filename)
                self.progress_window.update_channel_label(f'Binary image')
                self._calculate_with_progressbar(image, self._calculate, image, material)

            if sub_images_num:
                image = FractalParamCalculator(self.filename)
                self.progress_window.update_channel_label(f'Image 1')
                self._calculate_with_progressbar(image, image.topothesy, int(sub_images_num))

            self.progress_window.destroy()
            del self.progress_window

    def _calculate_with_progressbar(self, fractal: FractalParamCalculator, target: Callable, *args, **kwargs) -> None:
        self.progress_window.progress_worker = ProgressWorker(target=target,
                                                              args=args, kwargs=kwargs)
        self.progress_window.progress_worker.start()
        num = 1

        while not self.progress_window.progress_worker.killed and self.progress_window.progress_worker.is_alive():
            self.progress_window.progressbar['value'] = fractal.progress
            self.progress_window.update_progress_value()

            if target.__name__ == 'topothesy' and fractal.sub_image_num == num:
                self.progress_window.update_channel_label(f'Image {num}')
                num += 1

            self.update()

    def _calculate(self, fractal: FractalParamCalculator, material_indicator: int) -> None:
        if material_indicator == 0:
            fractal.invert_image_color()

        self.fitting = set()

        for name, state in self.parameters_frame.states.items():
            if state.get():
                self.progress_window.update_process_label(name)
                if name == 'capacity':
                    self.fitting.add(name)
                    fractal.capacity_fd()

                elif name == 'correlation':
                    self.fitting.add(name)
                    fractal.correlation_fd()

                elif name == 'Hurst exp.':
                    repetitions = self.parameters_frame.repetitions_value.get()
                    fractal.hurst_exponent(repetitions)

                elif name == 'lacunarity':
                    box_step = self.parameters_frame.box_step_value.get()
                    fractal.lacunarity(box_step)

                elif name == 'succolarity':
                    start = self.parameters_frame.start_position_value.get()
                    flooding_direction = self.parameters_frame.flooding_direction.get()
                    angle = {'top': 't2b', 'bottom': 'b2t', 'left': 'l2r', 'right': 'r2l', 'all': ['t2b', 'b2t', 'l2r',
                                                                                                   'r2l']}
                    if flooding_direction == 'all':
                        for direction in angle[flooding_direction]:
                            fractal.succolarity(start, direction)
                    else:
                        fractal.succolarity(start, angle[flooding_direction])

    def _validate_spinbox(self) -> bool:
        for name, state in self.parameters_frame.states.items():
            if state.get():
                if name == 'Hurst exp.':
                    repetitions = self.parameters_frame.repetitions_value.get()
                    lower = self.parameters_frame.repetitions['from']
                    upper = self.parameters_frame.repetitions['to']
                    if not validate_range(repetitions, lower, upper, 'Number of repetitions'):
                        return False

                elif name == 'lacunarity':
                    box_step = self.parameters_frame.box_step_value.get()
                    lower = self.parameters_frame.box_step['from']
                    upper = self.parameters_frame.box_step['to']
                    if not validate_range(box_step, lower, upper, 'Box step'):
                        return False

                elif name == 'succolarity':
                    start = self.parameters_frame.start_position_value.get()
                    lower = self.parameters_frame.start_position['from']
                    upper = self.parameters_frame.start_position['to']
                    if not validate_range(start, lower, upper, 'Start position'):
                        return False
                elif name == 'topothesy':
                    m = self.parameters_frame.division_factor_value.get()
                    lower = self.parameters_frame.division_factor_entry['from']
                    upper = self.parameters_frame.division_factor_entry['to']
                    if not validate_range(m, lower, upper, 'Image division factor'):
                        return False
        return True

    def _validate_topothesy(self) -> int:
        division_factor = self.parameters_frame.division_factor_value.get()
        sub_images_num = division_factor ** (1 / 2)

        # Testing whether the square root of the number is an integer
        if sub_images_num != int(sub_images_num):
            showwarning(title='Warning', message=f'Cannot divide the image to {division_factor} '
                                                 f'sub-images')
            sub_images_num = 0

        return sub_images_num


class ParametersFrame(ttk.LabelFrame):
    def __init__(self, master):
        super().__init__(master)
        self['text'] = 'Fractal parameters'
        valid = self.register(validate_number)

        for row in range(6):
            self.rowconfigure(row, weight=1)

        check_button_pad = 10
        self.states = {}

        self.states['capacity'] = tk.BooleanVar()
        self.capacity_fd = ttk.Checkbutton(self, text='Capacity fractal dimension', variable=self.states['capacity'])
        self.capacity_fd.grid(row=0, column=0, sticky='sew', padx=check_button_pad, columnspan=3, pady=PAD_Y)

        self.states['correlation'] = tk.BooleanVar()
        self.corr_fd = ttk.Checkbutton(self, text='Correlation fractal dimension', variable=self.states['correlation'])
        self.corr_fd.grid(row=1, column=0, sticky='sew', padx=check_button_pad, columnspan=3, pady=PAD_Y)

        self.states['Hurst exp.'] = tk.BooleanVar()
        self.hurst = ttk.Checkbutton(self, text='Hurst exponent', variable=self.states['Hurst exp.'])
        self.hurst.grid(row=2, column=0, sticky='w', padx=check_button_pad, pady=PAD_Y)

        repetitions_label = ttk.Label(self, text='repetitions - ')
        repetitions_label.grid(row=2, column=1, sticky='e', pady=PAD_Y)

        self.repetitions_value = tk.IntVar(value=1)
        self.repetitions = ttk.Spinbox(self, width=5, from_=1, to=1000, textvariable=self.repetitions_value, wrap=True)
        self.repetitions.grid(row=2, column=2, sticky='w', pady=PAD_Y)
        self.repetitions.config(validate="key", validatecommand=(valid, '%S'))

        self.states['lacunarity'] = tk.BooleanVar()
        self.lacunarity = ttk.Checkbutton(self, text='Lacunarity', variable=self.states['lacunarity'])
        self.lacunarity.grid(row=3, column=0, sticky='w', padx=check_button_pad, pady=PAD_Y)

        box_step_label = ttk.Label(self, text='box step - ')
        box_step_label.grid(row=3, column=1, sticky='e', pady=PAD_Y)

        self.box_step_value = tk.IntVar(value=1)
        self.box_step = ttk.Spinbox(self, width=5, textvariable=self.box_step_value, wrap=True)
        self.box_step.grid(row=3, column=2, sticky='w', pady=PAD_Y)
        self.box_step.config(validate="key", validatecommand=(valid, '%S'))

        self.states['succolarity'] = tk.BooleanVar()
        self.succolarity = ttk.Checkbutton(self, text='Succolarity', variable=self.states['succolarity'])
        self.succolarity.grid(row=4, column=0, sticky='w', padx=check_button_pad, pady=PAD_Y)

        start_position_label = ttk.Label(self, text='start - ')
        start_position_label.grid(row=4, column=1, sticky='e', pady=PAD_Y)

        self.start_position_value = tk.IntVar()
        self.start_position = ttk.Spinbox(self, width=5, textvariable=self.start_position_value)
        self.start_position.grid(row=4, column=2, sticky='w', pady=PAD_Y)
        self.start_position.config(validate="key", validatecommand=(valid, '%S'))

        flooding_direction_label = ttk.Label(self, text=' direction - ')
        flooding_direction_label.grid(row=4, column=3, sticky='w', pady=PAD_Y)

        self.flooding_direction = tk.StringVar()
        flooding_direction = ttk.Combobox(self, textvariable=self.flooding_direction, width=6)
        flooding_direction['values'] = ('top', 'bottom', 'left', 'right', 'all')
        flooding_direction['state'] = 'readonly'
        flooding_direction.set('left')
        flooding_direction.grid(row=4, column=4, sticky='w', pady=PAD_Y, padx=(0, PAD_X))

        self.states['topothesy'] = tk.BooleanVar()
        self.topothesy = ttk.Checkbutton(self, text='Topothesy', variable=self.states['topothesy'])
        self.topothesy.grid(row=5, column=0, sticky='w', padx=check_button_pad, pady=PAD_Y)

        division_factor = ttk.Label(self, text='divide to - ')
        division_factor.grid(row=5, column=1, sticky='e', pady=PAD_Y)

        self.division_factor_value = tk.IntVar()
        self.division_factor_entry = ttk.Spinbox(self, width=5, textvariable=self.division_factor_value, wrap=True)
        self.division_factor_entry.grid(row=5, column=2, sticky='w', pady=PAD_Y)
        self.division_factor_entry.config(validate="key", validatecommand=(valid, '%S'))

        # TODO: Add automatic calculations of topothesy based on the selected range.
        # values_range = ttk.Label(self, text='range - ')
        # values_range.grid(row=5, column=3, sticky='e', pady=PAD_Y)
        #
        # self.lower_range_value = tk.IntVar()
        # self.lower_range = ttk.Spinbox(self, width=5, textvariable=self.lower_range_value)
        # self.lower_range.grid(row=5, column=4, sticky='w', pady=PAD_Y)
        # self.lower_range.config(validate="key", validatecommand=(valid, '%S'))
        #
        # self.upper_range_value = tk.IntVar()
        # self.upper_range = ttk.Spinbox(self, width=5, textvariable=self.upper_range_value)
        # self.upper_range.grid(row=5, column=5, sticky='w', pady=PAD_Y)
        # self.upper_range.config(validate="key", validatecommand=(valid, '%S'))

    def update_spinbox(self, image: Image) -> None:
        self.division_factor_entry['from'] = 1
        self.division_factor_entry['to'] = min(image.size) ** 2

        self.start_position['from'] = 0
        self.start_position['to'] = min(image.size)

        self.box_step['from'] = 1
        self.box_step['to'] = min(image.size)


class ChannelsFrame(ttk.LabelFrame):
    def __init__(self, master):
        super().__init__(master)
        self['text'] = 'Number of channels'

        for col_n in range(5):
            self.columnconfigure(col_n, weight=1)

        self.rowconfigure(0, weight=1)
        self.number_of_channels_value = tk.IntVar()
        self.number_of_channels_value.set(1)

        binary = ttk.Radiobutton(self, text="Binary", value=1, variable=self.number_of_channels_value)
        binary.grid(row=0, column=0, pady=PAD_Y, padx=(PAD_X, 0))

        _4_channels = ttk.Radiobutton(self, text=str(4), value=4, variable=self.number_of_channels_value)
        _4_channels.grid(row=0, column=1, pady=PAD_Y)

        _8_channels = ttk.Radiobutton(self, text=str(8), value=8, variable=self.number_of_channels_value)
        _8_channels.grid(row=0, column=2, pady=PAD_Y, )

        _16_channels = ttk.Radiobutton(self, text=str(16), value=16, variable=self.number_of_channels_value)
        _16_channels.grid(row=0, column=3, pady=PAD_Y)

        _32_channels = ttk.Radiobutton(self, text=str(32), value=32, variable=self.number_of_channels_value)
        _32_channels.grid(row=0, column=4, pady=PAD_Y)

        _64_channels = ttk.Radiobutton(self, text=str(64), value=64, variable=self.number_of_channels_value)
        _64_channels.grid(row=0, column=5, pady=PAD_Y, padx=(0, PAD_X))


class MaterialFrame(ttk.LabelFrame):
    def __init__(self, master):
        super().__init__(master)
        self['text'] = 'Material indication'

        for col_n in range(2):
            self.columnconfigure(col_n, weight=1)

        self.rowconfigure(0, weight=1)
        self.material_indicator = tk.IntVar()

        black = ttk.Radiobutton(self, text='Black', value=0, variable=self.material_indicator)
        black.grid(row=0, column=0, pady=PAD_Y)

        white = ttk.Radiobutton(self, text="White", value=1, variable=self.material_indicator)
        white.grid(row=0, column=1, pady=PAD_Y)


class ImageFrame(ttk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.fractal: Optional[Image] = None
        self.photo_image: Optional[ImageTk.PhotoImage] = None

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)
        self.grid_propagate(0)

        button_file_explorer = ttk.Button(self, text='Browse files', command=master.browse_files)
        button_file_explorer.grid(row=0, column=1, sticky='nsew', padx=(PAD_X, 0), pady=(0, PAD_Y))

        self.label_file_explorer = ttk.Label(self, text='Select file', width=1)
        self.label_file_explorer.grid(row=0, column=0, sticky='nsew', pady=(0, PAD_Y))

        self.image_label = ttk.Label(self, padding=5, image=self.photo_image, anchor=tk.CENTER)
        self.image_label.grid(row=1, column=0, columnspan=2, sticky='nsew')
        self.image_label.propagate(0)

        self.greyscale_button = ttk.Button(self, text='Greyscale', state='disabled',
                                           command=lambda: self.change_image(self.fractal.convert('L')))

        self.greyscale_button.grid(row=2, column=0, sticky='nsew', pady=(PAD_Y, 0), padx=(0, PAD_X))

        self.normal_button = ttk.Button(self, text='Normal', state='disabled',
                                        command=lambda: self.change_image(self.fractal))

        self.normal_button.grid(row=2, column=1, sticky='nsew', pady=(PAD_Y, 0))

    def change_image(self, image: Image) -> None:
        self.photo_image = ImageTk.PhotoImage(image)
        self.image_label['image'] = self.photo_image


class ProgressWindow(tk.Toplevel):
    def __init__(self, master):
        super().__init__(master)

        window_width = 330
        window_height = 160

        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)

        self.columnconfigure(0, weight=2)
        self.columnconfigure(1, weight=3)
        self.columnconfigure(2, weight=1)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)

        self.geometry(f'+{center_x}+{center_y}')
        self.iconbitmap('logo.ico')
        self.resizable(False, False)
        self.title('Calculation progress')

        self.grab_set()

        self.progressbar = ttk.Progressbar(self, orient='horizontal', mode='determinate', length=window_width)
        self.progressbar.grid(row=0, column=0, columnspan=3, padx=PAD_X * 3, pady=PAD_Y * 3)

        self.channel_label = ttk.Label(self, anchor=tk.E, width=1)
        self.channel_label.grid(row=1, column=0, padx=(PAD_X * 3, 0), pady=(0, PAD_Y * 3), sticky='nsew')

        self.process_label = ttk.Label(self, text=f"Starting...", anchor=tk.CENTER, width=1)
        self.process_label.grid(row=1, column=1, pady=(0, PAD_Y * 3), sticky='nsew')

        self.value_label = ttk.Label(self, anchor=tk.W, width=1)
        self.value_label.grid(row=1, column=2, padx=(0, PAD_X * 3), pady=(0, PAD_Y * 3), sticky='nsew')

        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        self.progress_worker: Optional[ProgressWorker] = None

    def update_channel_label(self, channel: str) -> None:
        self.channel_label['text'] = f"{channel}"

    def update_process_label(self, method: str) -> None:
        self.process_label['text'] = f"{method} progress:"

    def update_progress_value(self) -> None:
        self.value_label['text'] = f"{self.progressbar['value']}%"

    def on_closing(self) -> None:
        if askokcancel("Quit", "Do you want to cancel calculations?"):
            self.progress_worker.kill()
            self.destroy()


class ProgressWorker(Thread):
    """
    Killing thread

    Code from - https://www.geeksforgeeks.org/python-different-ways-to-kill-a-thread/
    """

    def __init__(self, *args, **keywords):
        Thread.__init__(self, *args, **keywords)
        self.killed = False

    def start(self):
        self.__run_backup = self.run
        self.run = self.__run
        Thread.start(self)

    def __run(self):
        sys.settrace(self.globaltrace)
        self.__run_backup()
        self.run = self.__run_backup

    def globaltrace(self, frame, event, arg):
        if event == 'call':
            return self.localtrace
        else:
            return None

    def localtrace(self, frame, event, arg):
        if self.killed:
            if event == 'line':
                raise SystemExit()
        return self.localtrace

    def kill(self):
        self.killed = True
