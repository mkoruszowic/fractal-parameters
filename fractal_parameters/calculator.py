import glob
import os
from collections.abc import Sized
from functools import partial
from multiprocessing import Pool
from pathlib import Path
from time import time, sleep
from typing import Callable, Iterable, Literal

import numpy as np
from PIL import Image, ImageDraw, ImageChops

from fractal_parameters.parameters import (capacity_fractal_dimension, correlation_fractal_dimension, hurst_exponent,
                                           succolarity, lacunarity, structure_function, )
from fractal_parameters.tools import rnd, read_data, find_linear_region

DIRECTION = Literal['l2r', 'r2l', 'b2t', 't2b']


# White = material
# Black = gap, hole
# Works for every parameter
class ImageEditor:
    def __init__(self, file: str) -> None:
        """
        Constructor

        :param file: Filename
        """

        path = Path(file)
        self.directory = str(path.parent)
        self.filename = path.name.split('.')[0]
        self.image = Image.open(file, 'r').convert('L')  # convert to greyscale
        self.col, self.row = self.image.size  # width, height
        self.type = ''

    def _binarize_by_threshold(self, lower_limit: int, upper_limit: int) -> Image:
        fun = lambda x: 255 if lower_limit <= x <= upper_limit else 0
        bw_image = self.image.point(fun, mode='1')
        return bw_image

    def invert_image_color(self) -> Image:
        """Invert image color"""
        self.image = ImageChops.invert(self.image)

    @classmethod
    def from_binary(cls, file: str) -> 'ImageEditor':
        """
        Change image to black and white

        :param file: Filename
        :return:
        """
        editor = cls(file)
        editor.image = editor.image.convert('1')
        editor.type = 'binary'
        return editor

    @classmethod
    def to_binary_channel(cls, file: str, lower_limit: int, upper_limit: int) -> 'ImageEditor':
        """
        Change image to black and white according to the threshold

        :param file: Filename
        :param lower_limit: Lower limit
        :param upper_limit: Upper limit
        :return: ImageEditor instance
        """
        editor = cls(file)
        editor.image = editor._binarize_by_threshold(lower_limit, upper_limit)
        editor.type = f'range_{lower_limit}_{upper_limit}'

        image_folder = os.path.join(editor.directory, editor.filename + '_channels')
        if not os.path.isdir(image_folder):
            os.makedirs(image_folder)

        filename = f'{editor.filename}_{editor.type}.png'
        file_directory = os.path.join(image_folder, filename)

        if not os.path.isfile(file_directory):
            editor.image.save(file_directory)

        return editor

    def flood_image(self, start: int = 0, direction: DIRECTION = 'l2r') -> Image:
        """
        Flood the image in the chosen direction with optional start flooding position

        :param start: Start position
        :param direction: Reversion angle
        :return: Flooded inverted Image ready for succolarity
        """
        mode = self.image.mode
        direction_options = {'l2r': 0, 'r2l': 180, 'b2t': -90, 't2b': 90}

        if direction in direction_options:
            angle = direction_options[direction]
        else:
            raise ValueError('Invalid direction')

        img_rotated = self.image.rotate(angle, expand=1)  # anticlockwise
        image = img_rotated.convert("RGB")
        pix = image.load()

        for row in range(self.row):
            if pix[start, row] == (0, 0, 0):
                ImageDraw.floodfill(image, (start, row), (255, 0, 255))

        for row in range(self.row):
            for col in range(self.col):
                if pix[col, row] == (255, 255, 255) or pix[col, row] == (0, 0, 0):
                    pix[col, row] = 0
                elif pix[col, row] == (255, 0, 255):
                    pix[col, row] = (255, 255, 255)

        # image = image.rotate(-angle, expand=1)  # Stays rotated becouse it's easier for succolarity

        if direction in ('r2l', 'b2t'):
            image = image.rotate(180, expand=1)
        return image.convert(mode)


class FractalParamCalculator(ImageEditor):
    """
    Fractal Parameters calculator
    """

    def __init__(self, file: str) -> None:
        super().__init__(file)
        self.sub_image_num = 0
        self.progress = 0

    def correlation_fd(self) -> None:
        r_max = rnd(np.sqrt(self.row ** 2 + self.col ** 2))
        rng = range(3, r_max + 1)
        header = 'ln(R),ln(C(R))\n'
        method = partial(correlation_fractal_dimension, image=self.image)
        function_name = 'correlation'
        self.mp(method, function_name, header, rng)
        self.fitting(function_name)

    def capacity_fd(self) -> None:
        start_step = rnd(self.row / 2)
        rng = range(start_step, 0, -1)
        header = 'Box size,ln(1/eps),ln(N)\n'
        method = partial(capacity_fractal_dimension, image=self.image)
        function_name = 'capacity'
        self.mp(method, function_name, header, rng)
        self.fitting(function_name)

    def hurst_exponent(self, cycle: int) -> None:
        method = partial(hurst_exponent, image=self.image)
        header = 'H,u(H)\n'
        self.mp(method, 'hurst_exponent', header, range(cycle))

    def lacunarity(self, box_step: int) -> None:
        box_size = range(box_step, min(self.col, self.row), box_step)
        method = partial(lacunarity, image=self.image)
        header = 'Box size,Lacunarity,ln(a),ln(lac)\n'
        self.mp(method, 'lacunarity', header, box_size)

    def succolarity(self, start: int = 0, direction: DIRECTION = 'l2r') -> None:
        flooded_image = self.flood_image(start, direction)
        box_size = [i for i in range(1, self.col) if self.col % i == 0]
        info = '_pos_' + str(start) + '_' + direction
        method = partial(succolarity, image=flooded_image)
        header = 'Factor of division,Succolarity\n'
        self.mp(method, 'succolarity', header, box_size, info=info)

    def topothesy(self, image_division_factor: int) -> None:
        # '- m + 1' becouse it's the length of sub image - starting pixel
        m = self.row // image_division_factor
        self.sub_image_num = 1
        for y in range(0, self.row - m + 1, m):
            for x in range(0, self.col - m + 1, m):
                tau = range(1, m)
                area = (x, y, x + m, y + m)  # It trims to m-1, so to get m size image 'm' is used
                area_str = f'_{area[0]}_{area[1]}_{area[2]}_{area[3]}'
                sub_image = self.image.crop(area)
                method = partial(structure_function, sub_image=sub_image)
                header = 'Tau,ln(tau),ln(S(tau))\n'
                self.mp(method, 'topothesy', header, tau, info=area_str)
                self.sub_image_num += 1

    def mp(self, method: Callable, function_name: str, header: str, rng: Sized, info: str = '') -> None:
        """
        Method enabling multiprocessing calculations.
        Obtained results are saved into csv file.

        :param method: Function/method
        :param function_name: Function/method name
        :param header: Header in csv file
        :param rng: Range of values that calculations will be proceeded
        :param info: Additional info that will be included in filename
        :return:
        """
        start = time()
        folder = 'Result_' + self.filename

        method_folder = os.path.join(self.directory, folder, function_name)
        if not os.path.isdir(method_folder):
            os.makedirs(method_folder)

        filename = self.filename + '_' + self.type + '_' + function_name + info + '.csv'
        file_directory = os.path.join(method_folder, filename)
        cpu_num = 1

        if os.cpu_count() > 2:
            cpu_num = os.cpu_count() - 1

        with Pool(processes=cpu_num) as pool:
            res = pool.map_async(method, rng, chunksize=1)
            while not res.ready():
                sleep(0.5)
                temp = round((1 - res._number_left / len(rng)) * 100, 2)
                if self.progress != temp:
                    self.progress = temp

            real_res = res.get()
            res = _results_to_str(real_res)

            with open(file_directory, 'w') as f:
                f.write(header)
                f.writelines(res)
            # print(time() - start)

        self.progress = 0

    def fitting(self, name: str) -> None:
        if 'correlation' in name:
            indexes = 1, 2
        elif 'capacity' in name:
            indexes = 2, 3
        else:
            return

        x_col, y_col = indexes
        folder = 'Result_' + self.filename
        created_file = f'{name}_{self.filename}_values.csv'
        path = os.path.join(self.directory, folder, name)

        # Doesn't work with mostly nonlinear data
        with open(os.path.join(path, created_file), 'w') as f:
            f.write('a,b,u(a),u(b),filename\n')
            for file in glob.glob(os.path.join(path, '*.csv')):
                if 'values' not in file:
                    x, y, _, _ = read_data(file, x_col, y_col)
                    a, b, u_a, u_b = find_linear_region(x, y)
                    filename = file.split('\\')[-1]
                    f.write(f'{a:.2f},{b:.2f},{u_a:.2f},{u_b:.2f},{filename}\n')


def _results_to_str(results: Iterable) -> map:
    def string_maker(values) -> str:
        string = ''
        if values:
            for index, val in enumerate(values, start=1):
                if np.isinf(val):
                    return ''
                if index != len(values):
                    string += str(val) + ','
                else:
                    string += str(val) + '\n'
        return string

    res = map(string_maker, results)
    return res


def channel_divisor(channels_number: int):
    divisor = int(256 / channels_number)
    for num in range(0, 256, divisor):
        yield num, num + divisor - 1
