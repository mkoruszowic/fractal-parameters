"""
Lacunarity calculations using box-gliding algorithm

White = material
Black = gap, hole
"""

from collections import Counter
from typing import Tuple

import numpy as np
from PIL import Image


def lacunarity(box_size: int, image: Image, glide_step: int = 1) -> Tuple[float]:
    """
    Lacunarity calculations - statistical approach
    White pixels = material

    :param box_size: Box size - a
    :param image: Image
    :param glide_step: Gliding step size
    :return: box_size, lacunarity, ln(box_size), ln(lacunarity)
    """

    # Doesn't matter if L or 1 becouse std is still the same
    pix = np.array(image)  # Order is reversed to yx
    col, row = image.size
    box_mass = []
    for y in range(0, row, glide_step):
        for x in range(0, col, glide_step):
            matrix = pix[y:y + box_size, x:x + box_size]

            c, d = matrix.shape
            if c * d == box_size ** 2:  # More precise for certain case
                box_mass.append(np.sum(matrix))
                # Dividing by c*d changes nothing as long as this is calculated using variance
            else:
                break

    value = ((np.std(box_mass) / np.mean(box_mass)) ** 2) + 1  # variance / mean^2
    return box_size, value, np.log(box_size), np.log(value)


def lacunarity_probability_dist(box_size: int, image: Image, glide_step: int = 1) -> Tuple[float]:
    """
    Lacunarity calculations - frequency distribution approach

    :param box_size: Box size - a
    :param image: Image
    :param glide_step: Gliding step size
    :return: box_size, lacunarity, ln(box_size), ln(lacunarity)
    """
    pix = np.array(image)
    col, row = image.size
    box_mass = []

    for y in range(0, row, glide_step):
        for x in range(0, col, glide_step):
            matrix = pix[y:y + box_size, x:x + box_size]
            c, d = matrix.shape
            if c * d == box_size ** 2:
                box_mass.append(np.sum(matrix))
            else:
                break

    number_p = Counter(box_mass)
    total = len(box_mass)  # Same length as lacunarity.py
    # Has to be that way to overcome RuntimeWarning in some big number
    wa_box_mass_dict = {p: p * num_p / total for p, num_p in number_p.items()}
    wa_box_mass = sum(value for value in wa_box_mass_dict.values())
    wa2_box_mass = sum(p * value for p, value in wa_box_mass_dict.items())

    value = wa2_box_mass / wa_box_mass ** 2
    return box_size, value, np.log(box_size), np.log(value)
