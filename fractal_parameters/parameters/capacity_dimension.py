from typing import Tuple

import numpy as np
from PIL import Image

from fractal_parameters.tools import rnd


# It differs from the matlab code probably becouse of different indexes
def capacity_fractal_dimension_OLD(wp: int, image: Image) -> Tuple[float, float, float]:
    """
    Capacity (box counting) fractal dimension calculations

    :param wp: The absolute length of the side of the epsilon cell
    :param image: Image
    :return: wp, ln(1/epsilon)+ln(L), ln(N), d_cap
    """
    pix = image.load()
    col, row = image.size

    step_kc = rnd((row / wp) - 1)
    step_lc = rnd((col / wp) - 1)
    row_B = step_kc
    col_B = step_lc

    B = np.zeros((row_B, col_B))
    for k in range(1, step_kc + 1):
        for l in range(1, step_lc + 1):
            white = 1
            for p in range(1 + wp * (k - 1), wp * (k - 1) + wp + 1):
                for q in range(1 + wp * (l - 1), wp * (l - 1) + wp + 1):
                    # It's never 1, change to np.matrix
                    if pix[q - 1, p - 1] == 255:  # in matlab it's like matrices that's why it's reversed
                        white = 1
                    else:
                        white = 0
            if white != 1:
                B[k - 1, l - 1] = 0
            else:
                B[k - 1, l - 1] = 1

    Nf = col_B * row_B
    white_B = sum(sum(B))
    black_B = Nf - white_B

    epsylon = wp / col
    x = np.log(1 / epsylon) + np.log(col)  # Shouldn't be ln10?
    return wp, x, np.log(black_B)


# Gives good result even for sierpinski carpet. Results differs from matlab function
def capacity_fractal_dimension(box_size: int, image: Image) -> Tuple[float, float, float]:
    """
    Capacity (box counting) fractal dimension calculations
    White pixels = material

    :param box_size: The absolute length of the side of the epsilon cell
    :param image: Image
    :return: wp, ln(1/epsilon)+ln(L), ln(N), d_cap
    """
    col, row = image.size
    pix = np.array(image)
    n = 0

    for x in range(0, col, box_size):
        for y in range(0, row, box_size):
            matrix = pix[y:y + box_size, x:x + box_size]
            if np.sum(matrix) >= 1:
                n += 1

    numerator = np.log(n)
    epsylon = box_size / col
    denominator = np.log(col) + np.log(1 / epsylon)

    return box_size, denominator, numerator
