from random import random
from typing import Tuple, Optional

import numpy as np
from PIL import Image

from fractal_parameters.tools import rnd, linear_regression


def hurst_exponent(_, image: Image) -> Optional[Tuple[float]]:
    """
    Hurst exponent calculation using random walking

    White = material
    Black = gap, hole

    :param _: Symbolic argument representing number of attempts - necessary from the point of multiprocessing
    :param image: Image
    :return: H, u(H)
    """
    A = image.load()
    col, row = image.size

    black = True

    # try:
    while black:
        a = rnd(random() * (row - 1))
        b = rnd(random() * (col - 1))
        if A[b, a] != 0:
            black = False
    # # If there is no white pixels
    # except IndexError:
    #     return

    repetitions = 1000
    x = []
    y = []

    for t in range(rnd(row / 15), rnd(row / 2)):
        kx0 = b
        ky0 = a
        D = []
        for rep in range(repetitions):
            bx = kx0
            by = ky0
            for t1 in range(t):
                ant = random()
                if 0 <= ant < 0.25 and 0 < (by - 1) < col - 1 and A[bx, by - 1] != 0:
                    bx = bx
                    by -= 1
                elif 0.25 <= ant < 0.5 and 0 < (bx - 1) < row - 1 and A[bx - 1, by] != 0:
                    bx -= 1
                    by = by
                elif 0.5 <= ant < 0.75 and 0 < (by + 1) < col - 1 and A[bx, by + 1] != 0:
                    bx = bx
                    by += 1
                elif 0.75 <= ant <= 1.0 and 0 < (bx + 1) < row - 1 and A[bx + 1, by] != 0:
                    bx += 1
                    by = by
                else:
                    bx = bx
                    by = by

            D.append((kx0 - bx) ** 2 + (ky0 - by) ** 2)

        d = sum(D) / repetitions  # Squared and averaged displacement over several repetitions
        dp = np.sqrt(d)  # Square root that you do not have to divide the slope by 2

        if dp > 1:
            x.append(np.log(t))
            y.append(np.log(dp))

    try:
        result, uncertainty = linear_regression(x, y)
    except TypeError:  # In case of 0
        return

    return result[0], uncertainty[0]
