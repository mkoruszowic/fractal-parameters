from typing import Tuple

import numpy as np
from PIL import Image


# No longer used
def structure_function_OLD(m: int, image: Image, i: int = 0, j: int = 0) -> Tuple[float]:
    """
    Structure function calculations - old approach
    Calculated for greyscale

    :param m: Sub-image size
    :param image: Image
    :param i: X - coordinate of cropped image
    :param j: Y - coordinate of cropped image
    :return:
    """
    pix = image.load()  # xy
    ln_values = []
    ln_tau = []

    for tau in range(1, m):
        structure_fun = 0
        for y in range(m - tau):
            for x in range(m - tau):
                value = abs(pix[x + i, y + j] - pix[x + i + tau, y + j + tau])
                structure_fun += value

        structure_fun /= (m - tau) ** 2
        ln_values.append(np.log(structure_fun))
        ln_tau.append(np.log(tau))
    return ln_tau, ln_values


def structure_function(tau: int, sub_image: Image) -> Tuple[float]:
    """
    Structure functions calculations
    Calculated for greyscale

    :param tau: Delay
    :param sub_image: Sub-image
    :return: tau, ln(tau), ln(structure_function)
    """
    m, _ = sub_image.size
    pix = sub_image.load()  # xy

    structure_fun = 0
    for y in range(m - tau):
        for x in range(m - tau):
            value = abs(pix[x, y] - pix[x + tau, y + tau])
            structure_fun += value

    structure_fun /= (m - tau) ** 2
    return tau, np.log(tau), np.log(structure_fun)


def determine_topothesy(slope: float, intercept: float) -> Tuple[float, float]:
    """
    Determination of topothesy

    :param slope: a
    :param intercept: b
    :return: Topothesy, fractal dimension
    """
    fractal_dimension = 2 - (slope / 2)
    topothesy = np.exp(intercept / (2 * fractal_dimension - 2))
    return topothesy, fractal_dimension
