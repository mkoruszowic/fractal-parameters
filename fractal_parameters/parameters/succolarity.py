from typing import Tuple

import numpy as np
from PIL import Image


# Mono required becouse of matrix where white = 1 if mono
def succolarity(box_size: int, image: Image) -> Tuple[int, float]:
    """
    Succolarity calculations for flooding from left to right. For other directions image need to be properly rotated.
    White pixels = material

    :param box_size: Size of box
    :param image: Image
    :return: Box size, succolarity
    """
    col, row = image.size
    pix = np.array(image)

    numerator = []
    denominator = []
    pressure = box_size / 2
    # x first becouse direction is from left to right
    for x in range(0, col, box_size):
        for y in range(0, row, box_size):
            matrix = pix[y:y + box_size, x:x + box_size]

            numerator.append((np.sum(matrix) / box_size ** 2) * pressure)
            denominator.append(pressure)

        pressure += box_size

    numerator = sum(numerator)
    denominator = sum(denominator)
    value = numerator / denominator

    return int(col / box_size), value
