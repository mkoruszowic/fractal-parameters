from random import random
from typing import Tuple, Any

import numpy as np
from PIL import Image


def correlation_fractal_dimension(r: int, image: Image) -> Tuple[Any, float]:
    """
    Correlation fractal dimension calculation
    White pixels = material

    :param r:  Circle radius
    :param image: Microscope image
    :return: ln(r), ln(C(r)), d_corr
    """
    A = image.load()
    col, row = image.size

    Nt = 0
    C_R = 0
    num_attempt = int(np.floor(np.sqrt(row * col)))
    x = []
    y = []

    for n in range(0, num_attempt):
        x_0 = np.floor(1 + random() * (col - 1))
        y_0 = np.floor(1 + random() * (row - 1))

        if A[x_0, y_0] != 0:  # Indicate white pixel
            Nt += 1
            x.append(x_0)
            y.append(y_0)

    for Np in range(0, Nt):
        for Nq in range(0, Nt):
            if (Np != Nq) and np.sqrt(
                    (x[Np] - x[Nq]) * (x[Np] - x[Nq]) + (y[Np] - y[Nq]) * (y[Np] - y[Nq])) < r:
                C_R += 1

    if Nt == 0:
        return np.log(r), np.inf

    return np.log(r), np.log(C_R / (Nt * Nt))
