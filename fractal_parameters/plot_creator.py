import glob
import os
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np

from fractal_parameters.tools import read_data

font_size = 13
tick_size = 13


def define_indexes(filename: str) -> Tuple[int, int]:
    data_indicator = 0, 0

    if 'correlation' in filename:
        data_indicator = 1, 2
    elif 'capacity' in filename:
        data_indicator = 2, 3
    elif 'lacunarity' in filename:
        data_indicator = 3, 4
    elif 'succolarity' in filename:
        data_indicator = 1, 2
    elif 'topothesy' in filename:
        data_indicator = 2, 3
    elif 'hurst' in filename:
        data_indicator = 1, 2

    return data_indicator


def channel_number_test(file: str):
    upper_limit = int(file.split('range_')[1].split('_')[1])
    return int(upper_limit)


def hurst_histogram_2d(file_path: str, number_of_channels: int):
    x_container = []
    channel_container = []
    x_col, y_col = define_indexes('hurst')
    plt.title(file_path.split('Result_')[-1], fontsize=font_size)

    hurst_files = glob.glob(os.path.join(file_path, 'hurst_exponent', '*.csv'))
    hurst_files.sort(key=channel_number_test)

    for channel, file in enumerate(hurst_files, start=1):
        x, _, _, _ = read_data(file, x_col, y_col)
        x_container.extend(x)
        channel_container.extend([channel] * len(x))

    plt.xlabel('Hurst exponent', fontsize=font_size)
    plt.ylabel('Channel', fontsize=font_size)
    plt.hist2d(x_container, channel_container, bins=[50, number_of_channels], cmap='Blues')
    plt.yticks(ticks=tuple(set(channel_container)), fontsize=tick_size)
    plt.xticks(fontsize=tick_size)
    colorbar = plt.colorbar()
    colorbar.ax.tick_params(labelsize=tick_size)
    plt.show()


def multiple_channel_plot(file_path: str, method: str):
    x_col, y_col = define_indexes(method)
    x_label = 'x'
    y_label = 'y'
    plt.title(file_path.split('Result_')[-1], fontsize=font_size)

    method_files = glob.glob(os.path.join(file_path, method, f'*{method}.csv'))
    method_files.sort(key=channel_number_test)

    for channel, file in enumerate(method_files, start=1):
        x, y, y_label, x_label = read_data(file, x_col, y_col)
        plt.plot(x, y, '-', label=f'Channel {channel}')

    plt.xlabel(x_label, fontsize=font_size)
    plt.ylabel(y_label, fontsize=font_size)

    plt.yticks(fontsize=tick_size)
    plt.xticks(fontsize=tick_size)

    plt.legend(bbox_to_anchor=(1.35, 0.5), loc='center right', fontsize=tick_size)
    plt.tight_layout()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(plt.FormatStrFormatter('%.2g'))
    plt.grid(True)
    plt.show()


def multiple_channel_plot_for_fitting(sample_path: str, method: str):
    plt.title(sample_path.split('\\')[-1], fontsize=font_size)
    channels_num = 1
    for file in glob.glob(os.path.join(f'{sample_path}', '*', method, '*values*.csv')):
        value, channels, _, _ = read_data(file, 1, 5)

        label = file.split('Result_')[1].split('\\')[0]

        channels_num = list(map(channel_number_test, channels))
        plt.plot(channels_num, value, 'o', label=label)

    plt.xlabel('Channel', fontsize=font_size)
    plt.ylabel(method, fontsize=font_size)

    plt.yticks(fontsize=tick_size)
    plt.xticks(channels_num, fontsize=tick_size)

    plt.legend(bbox_to_anchor=(1.35, 0.5), loc='center right', fontsize=tick_size)
    plt.tight_layout()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(plt.FormatStrFormatter('%.2g'))
    plt.grid(True)

    plt.show()


def mean_succolarity_plot(file_path: str, number_of_channels: int):
    method = 'succolarity'
    x_col, y_col = define_indexes(method)
    x_label = 'x'
    y_label = 'y'
    plt.title(file_path.split('Result_')[-1], fontsize=font_size)
    container = {}
    x = 0
    succolarity_files = glob.glob(os.path.join(file_path, method, f'*{method}*.csv'))
    succolarity_files.sort(key=channel_number_test)

    for channel in range(1, number_of_channels + 1):
        file_list = succolarity_files[:4]
        del succolarity_files[:4]
        mean = np.array(0.)

        for file in file_list:
            x, y, y_label, x_label = read_data(file, x_col, y_col)
            y = np.array(y)
            mean = mean + y
        container[channel] = mean / number_of_channels

    for channel, succolarity in container.items():
        plt.plot(x, succolarity, 'o-', label=f'Channel {channel}')
        print(succolarity)

    plt.xlabel(x_label, fontsize=font_size)
    plt.ylabel('Mean ' + y_label, fontsize=font_size)

    plt.yticks(fontsize=tick_size)
    plt.xticks(fontsize=tick_size)

    plt.legend(bbox_to_anchor=(1.35, 0.5), loc='center right', fontsize=tick_size)
    plt.tight_layout()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(plt.FormatStrFormatter('%.2g'))
    plt.grid(True)

    plt.show()


def all_direction_succolarity_plot(file_path: str, channel_number: int):
    method = 'succolarity'
    x_col, y_col = define_indexes(method)
    x_label = 'x'
    y_label = 'y'
    plt.title(file_path.split('Result_')[-1] + f'- channel {channel_number}', fontsize=font_size)
    succolarity_files = glob.glob(os.path.join(file_path, method, f'*{method}*.csv'))
    succolarity_files.sort(key=channel_number_test)

    channel_number -= 1
    channel = succolarity_files[channel_number * 4:channel_number * 4 + 4]

    for file in channel:
        x, y, y_label, x_label = read_data(file, x_col, y_col)
        label = file.rstrip('.csv')[-3:]
        plt.plot(x, y, 'o-', label=label)

    plt.xlabel(x_label, fontsize=font_size)
    plt.ylabel(y_label, fontsize=font_size)

    plt.yticks(fontsize=tick_size)
    plt.xticks(fontsize=tick_size)

    plt.legend(bbox_to_anchor=(1.25, 0.5), loc='center right', fontsize=tick_size)
    plt.tight_layout()
    ax = plt.gca()
    ax.yaxis.set_major_formatter(plt.FormatStrFormatter('%.2g'))
    plt.grid(True)

    plt.show()


if __name__ == '__main__':
    sample_path = '..\\..\\..\\..\\Samples\\sample_3'
    sample_name = 'Result_sample_3_step1'
    # sample_path = '..\\Results and samples'
    # sample_name = 'Result_pan_14_to_gs'
    file_path = os.path.join(sample_path, sample_name)

    # hurst_histogram_2d(file_path, number_of_channels=8)  # hurst exponent
    # multiple_channel_plot(file_path, 'lacunarity')  # lacunarity
    # multiple_channel_plot_for_fitting(sample_path, 'capacity')  # capacity, correlation, topothesy
    # mean_succolarity_plot(file_path, 8)  # succolarity
    # all_direction_succolarity_plot(file_path, 5)
