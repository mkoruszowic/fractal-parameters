# Fractal parameters
Python version: 3.8

##### Creating virtual environment for local development:
1. Open command prompt in project directory
2. Enter:
```
$ <python_path>python -m pip install virtualenv
$ virtualenv <venv_name>
$ <venv_name>\Scripts\python -m pip install -r requirements.txt
```

##### Running program:
1. Open command prompt in project directory
2. Enter:
```
$ <venv_name>\Scripts\activate
$ python run.py
```